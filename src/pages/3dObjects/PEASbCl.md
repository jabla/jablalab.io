---
title: "3DObj"
pubDate: "2024-03-13"
---
<script type="module" src="https://ajax.googleapis.com/ajax/libs/model-viewer/3.4.0/model-viewer.min.js"></script>
<div class="viewer" style="width: 100vw; height: 100vh;">
<model-viewer 
  style="width: 90vw; height: 90vh;"
  src="/assets/images/3dObjects/obj1.glb" 
  ar ar-modes="webxr scene-viewer quick-look" 
  scale = "2 2 2"
  camera-controls
  xr-environment>
  <button slot="ar-button" style="background-color: white; border-radius: 4px; border: none; position: absolute; top: 16px; right: 16px; ">
      👋 Activate AR
  </button>
</model-viewer>
</div>