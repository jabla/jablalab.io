---
layout: '@/templates/BasePost.astro'
title: "Useful software during my PhD"
description: "Some software I am using during my PhD"
pubDate: "2022-05-13"
tags: ["python", "matplotlib", "zotero", "obsidian"]
---


# Useful software during my PhD
During my PhD I have used several programs to collect, analyze and organize data and knowledge. During this process I found for me important aspects in the tools I want to use. I want to use operating system agnostic tools as much as possible and generate documents and data files which can be used with different software in the case I switch the program. 
The following is a short list of tools I am currently using. 


## Reference manager
In science it is important to keep track of the research paper you read and which information they contain. Therefore, I started using a reference manager.
I started out using [Citavi](citavi.com) as the license was provided by my university. But it always felt slow and writing in LaTex was not so easy. Then I looked into [JabRef](https://www.jabref.org/) a reference manager with focus on Latex. 
It was quite good until I checked out [Zotero](https://zotero.com). Zotero ticks a lot of the boxes I want for my reference manager and in combination with community plugins it is very powerful. I use the BetterBibTex and Zotfile plugins. BetterBibTex helps with bib files and writing in LaTex. ZotFile is useful for renaming files and in my case to specify the location of the files as I sync my files using Nextcloud. With the newest version there is also a PDF reader included which makes it possible to annotate PDFs in Zotero and then export the annotations.


## Knowledge organisation
To organise my notes from meetings and papers I started to use [Notion](https://notion.so). At the time I used it there was no way to export the notes in a good way which limited the use for me and it felt like I was not in control of my notes. Then I learnt about wikis like vimwiki and I ended up with [obsidian.md](https://obsidian.md). It is basically a markdown editor which ticks most of the boxes in knowledge management software. The notes are stored in plain markdown which makes it possible to use other software to edit and read the notes. The software just gives the users a nice interface. Furthermore, there are a lot of community plugins which can enhance the functionalities. I store my obsidian vault in my nextcloud but also use git for version control.

## Data visualisation

During my bachelor and master thesis I used Origin to plot my data as it was the software used by my supervisors. For me a disadvantage of the software was the time it took to get visually pleasing plots and get a consistent style for all the plots.
During the first Corona lockdown I used my time to get more familiar with [python](https://python.org) and [matplotlib](https://matplotlib.org/). Now I have little script where I can just paste in the new data and always get same looking plots. 
For collaborative projects everybody can use the same style sheet and produce plots which have the same style.
Furthermore, it is possible to use a version control system like [git](https://git-scm.com) to track changes in your work.

## Paper writing

For writing I like to use latex whenever it is possible. Tools like [overleaf.com](https://overleaf.com) make collaborative writing easy. Everybody can edit the document at the same time and you can see the changes they make.
Unfortunately some scientific journals only accept word documents so I have to write the articles in word.
Currently, I am also experimenting with writing in markdown because I use it with obsidian and it is easy and fast to write texts.

