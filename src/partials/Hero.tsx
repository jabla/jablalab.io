import {
  GradientText,
  HeroAvatar,
  Section,
} from 'astro-boilerplate-components';

const Hero = () => (
  <Section>
    <HeroAvatar
      title={
        <>
          Hi there, I'm <GradientText>Jakob</GradientText> 🧪
        </>
      }
      description={
        <>
          As a doctoral researcher at  the {' '}
          <a href="https://www.fkf.mpg.de/en">MPI for Solid State Research</a>{' '}
          in the group of{' '}
          <a href="https://www.fkf.mpg.de/lotsch">Prof. Bettina Lotsch</a> in Stuttgart, I am exploring the chemical space of hybrid organic inorganic metal halides. 
         My research focuses on understanding and controlling the unique properties of these materials, particularly how the lone pair expression leads to ferroelectric and luminescent properties.
          Using a combination of advanced characterization techniques – including single crystal X-ray diffraction, powder diffraction, differential thermal analysis, and various spectroscopic methods – I investigate how organic cations can template both the electronic and structural properties of these hybrid materials. 
Currently, I also explore the use of machine learning assisted approaches to investigate these templating effects.
          This systematic approach allows me to establish clear structure-property relationships and design materials with tailored functionalities.


         Besides my research I am a member of the {' '}
          <a href="https://www.e-conversion.de/about-e-conversion/graduate-program/">
            eConversion Graduate Program
          </a>{' '}
          and from 2020 to 2022 I have been a member of the student board, organizing educational and networking events.

          In 2022 I was an organizer of{' '}
          <a href="https://www.inascon.org"> INASCON2022 in Munich</a>, where I managed the conference website, publich outreach, and general event coordination.. <br></br>
          

          <br></br>
          <br></br>
          <ul>
            <li>
              {' '}
              2021 -- 2022 Organizer of{' '}
              <a href="https://www.inascon.org"> INASCON2022 in Munich</a>
            </li>
            <li>
              {' '}
              2020 -- 2022 Member of the student board of the{' '}
              <a href="https://www.e-conversion.de/about-e-conversion/graduate-program/">
                eConversion Graduate Program
              </a>
            </li>
            <li>2019 Master Thesis in the group of Prof. Bettina Lotsch</li>
            <li>
              2017 Research internship with{' '}
              <a href="https://kemi.uu.se/angstrom/research/inorganic-chemistry">
                Prof. Gunnar Westin at Uppsala University
              </a>
            </li>
            <li>
              2016 Research internship with the group of{' '}
              <a href="https://www.cup.uni-muenchen.de/ac/schnick/">
                Prof. Wolfgang Schnick at LMU Munich
              </a>
            </li>
            <li>
              2016 -- 2019 MSc in Chemistry at{' '}
              <a href="http://cup.uni-muenchen.de/">LMU Munich</a>
            </li>
            <li>2016 Bachelor Thesis in the group of Prof. Bettina Lotsch </li>
            <li>
              2012 -- 2016 BSc in Chemistry at{' '}
              <a href="http://cup.uni-muenchen.de/">LMU Munich</a>
            </li>
          </ul>
        </>
      }
    />
  </Section>
);

export { Hero };
