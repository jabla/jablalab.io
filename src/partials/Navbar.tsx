import {
  Logo,
  NavbarTwoColumns,
  NavMenu,
  NavMenuItem,
  Section,
} from 'astro-boilerplate-components';

const Navbar = () => (
  <Section>
    <NavbarTwoColumns>
      <a href="/">
        <Logo
          icon={
            <svg
              className="mr-1 h-10 w-10 stroke-cyan-600"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="none"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            >
              <path d="M0 0h24v24H0z" stroke="none"></path>
              <rect x="3" y="12" width="6" height="8" rx="1"></rect>
              <rect x="9" y="8" width="6" height="12" rx="1"></rect>
              <rect x="15" y="4" width="6" height="16" rx="1"></rect>
              <path d="M4 20h14"></path>
            </svg>
          }
          name="Jakob's Website"
        />
      </a>

      <NavMenu>
        <NavMenuItem href="/posts">Blog</NavMenuItem>
        <NavMenuItem href="https://gitlab.com/jabla" target="_blank">
          GitLab
        </NavMenuItem>
        <NavMenuItem href="https://twitter.com/jakob_bl" target="_blank">
          Twitter
        </NavMenuItem>
        <NavMenuItem
          href="https://scholar.google.de/citations?user=EaEo2t4AAAAJ&hl=de"
          target="_blank"
        >
          GoogleScholar
        </NavMenuItem>
        <NavMenuItem
          href="https://www.linkedin.com/in/jakob-blahusch-199443132"
          target="_blank"
        >
          LinkedIn
        </NavMenuItem>
        <NavMenuItem href="/legal/impressum">Impressum</NavMenuItem>
      </NavMenu>
    </NavbarTwoColumns>
  </Section>
);

export { Navbar };
