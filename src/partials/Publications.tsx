import {
  ColorTags,
  GradientText,
  Project,
  Section,
  Tags,
} from "astro-boilerplate-components";

const Publications = () => (
  <Section
    title={
      <>
        <GradientText>Publications</GradientText>
      </>
    }
  >
    <div className="flex flex-col gap-6">
      <Project
        name="Beyond Templating: Electronic Structure Impacts of Aromatic Cations in Organic–Inorganic Antimony Chlorides"
        description="Blahusch, J.; Fabini, D. H.; Jiménez-Solano, A.; Lotsch, B. V., Zeitschrift für anorganische und allgemeine Chemie 2021, 647 (8), 857–866."
        link="https://doi.org/10.1002/zaac.202000484"
        img={{
          src: "/assets/images/octahedra.svg",
          alt: "Octahedra",
        }}
        category={
          <>
            <Tags color={ColorTags.FUCHSIA}>Antimony Halides</Tags>
          </>
        }
      />
      <Project
        name="Chemical Stability and Ionic Conductivity of LGPS-Type Solid Electrolyte Tetra-Li7SiPS8 after Solvent Treatment"
        description="Hatz, A.-K.; Calaminus, R.; Feijoo, J.; Treber, F.; Blahusch, J.; Lenz, T.; Reichel, M.; Karaghiosoff, K.; Vargas-Barbosa, N. M.; Lotsch, B. V., ACS Appl. Energy Mater. 2021, 4 (9), 9932–9943."
        link="https://doi.org/10.1021/acsaem.1c01917"
        img={{
          src: "/assets/images/battery.svg",
          alt: "Octahedra",
        }}
        category={
          <>
            <Tags color={ColorTags.VIOLET}>Electrolytes</Tags>
          </>
        }
      />
      <Project
        name="Pyrrolation of Melem: A Facile Gateway into the Field of Monomeric s-Heptazine Chemistry"
        description="Koller, T. J.; Witthaut, K.; Wolf, F.; Singer, J. N.; Blahusch, J.; Li, C.; Valsamidou, V.; Johrendt, D.; Schnick, W., Chemistry A European Journal 2025"
        link="https://chemistry-europe.onlinelibrary.wiley.com/doi/abs/10.1002/chem.202500271"
        img={{}}
        category={
          <>
            <Tags color={ColorTags.VIOLET}>Carbon Nitrides</Tags>
          </>
        }
      />
      <Project
        name="Predict Before You Precipitate: Learning Templating Effects in HOI Antimony and Bismuth Halides"
        description="Blahusch, J.; Jakob, K. S..;Margraf, J. T.; Reuter, K.; Lotsch, B. V., ChemRxiv 2025"
        link="https://doi.org/10.26434/chemrxiv-2025-1s2mm"
        img={{
          src: "/assets/images/octahedra.svg",
          alt: "Octahedra",
        }}
        category={
          <>
            <Tags color={ColorTags.VIOLET}>Antimony Halides</Tags>
          </>
        }
      />
    </div>
  </Section>
);

export { Publications };
