export const AppConfig = {
  site_name: 'Jakob Blahusch',
  title: 'Personal Website',
  description: 'Personal Website',
  author: 'Jakob',
  locale_region: 'en-us',
  locale: 'en',
};
